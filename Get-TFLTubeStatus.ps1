function Get-TFLTubeStatus {

    <#

    .SYNOPSIS
    Retrieves TFL tube statuses.

    .DESCRIPTION
    Retrieves status for a given tube line, future status for a given tube line, and all current disruptions.

    .PARAMETER Line
    Specifies the tube line to check the status of.

    .PARAMETER StartDate
    Specifies the start date range to check future disruptions.  Format: YYYY-MM-DD

    .PARAMETER EndDate
    Specifies the end date range to check future disruptions.  Format:  YYYY-MM-DD

    .PARAMETER Disruption
    Retrieves all current tube disruptions.

    .INPUTS
    None.

    .OUTPUTS
    System.Management.Automation.PSCustomObject.

    .EXAMPLE
    Get-TFLTubeStatus -Line Piccadilly

    .EXAMPLE
    Get-TFLTubeStatus -Line Piccadilly -StartDate 2022-10-24 -EndDate 2022-11-08

    .EXAMPLE
    Get-TFLTubeStatus -Disruption

    .NOTES
    Date input format: YYYY-MM-DD
    Tested working with:

    Name                           Value
    ----                           -----
    PSVersion                      7.2.7
    PSEdition                      Core

    #>

    [cmdletbinding()]

    param(

        [parameter(Mandatory = $true, ParameterSetName = 'Line')]
        [ValidateSet('Bakerloo', 'Central', 'Circle', 'District', 'Elizabeth', 'Hammersmith-City', 'Jubilee', 'Metropolitan', 'Northern', 'Piccadilly', 'Victoria', 'Waterloo-City')]
        [System.String]$Line,
        [System.String]$StartDate,
        [System.String]$EndDate,

        [parameter(Mandatory = $true, ParameterSetName = 'Disruption')]
        [Switch]$Disruption

    ) # param

    $TFLTubeStatus = @()
    $TFLTubeStatusRange = @()
    $TFLTubeDisruptions = @()

    $Uri = "https://api.tfl.gov.uk/Line/"

    switch ($PSBoundParameters.keys) {

        'Line' {

            $Uri += "$($Line.ToLower())/status/"

            Write-Debug "`$Uri set to $Uri"

            $LineResult = Invoke-RestMethod -Uri $Uri

            if (!($LineResult.lineStatuses.reason)) {
                $Reason = 'N/A'
            } # if
            else {
                $Reason = $LineResult.lineStatuses.reason
            } # else

            $TFLTubeStatus += [PSCustomObject]@{
                Line   = $Line
                Status = $LineResult.lineStatuses.statusSeverityDescription
                Reason = $Reason
            } # PSCustomObject

        } # Line

        'StartDate' {

            if (!($EndDate)) {
                Write-Error 'Please specify an end date using the EndDate parameter.' -ErrorAction Stop
            } # if

            $Uri += "$StartDate/to/$EndDate/"

            Write-Debug "`$Uri set to $Uri"

            try {
                $Results = Invoke-RestMethod -Uri $Uri
            } # try
            catch [Microsoft.PowerShell.Commands.HttpResponseException] {
                Write-Error 'Please confirm you are using the correct date format.  See help file for more info.' -ErrorAction Stop
            } # catch

            if ($Results.linestatuses.disruption) {

                foreach ($Result in $Results.linestatuses) {

                    foreach ($ValidityPeriod in $Result.validityPeriods) {

                        $TFLTubeStatusRange += [PSCustomObject]@{
                            Line               = $Line
                            DisruptionFromDate = $ValidityPeriod.fromDate
                            DisruptionToDate   = $ValidityPeriod.toDate
                            DisruptionCategory = $Result.disruption.category
                            Description        = $Result.disruption.description
                            AdditionalInfo     = $Result.disruption.additionalInfo
                        } # PSCustomObject

                    } # foreach

                } # foreach

            } # if
            else {

                $TFLTubeStatusRange += [PSCustomObject]@{
                    Line        = $Line
                    Description = "No planned disruptions between $StartDate - $EndDate."

                } # PSCustomObject

            } # else

        } # StartDate

        'EndDate' {

            if (!($StartDate)) {
                Write-Error 'Please specify a start date using the StartDate parameter.' -ErrorAction Stop
            } # if

        } # EndDate

        'Disruption' {

            $Results = Invoke-RestMethod -Uri 'https://api.tfl.gov.uk/Line/Mode/tube/Status/'

            foreach ($Result in $Results) {

                $TFLTubeDisruptions += [PSCustomObject]@{
                    Line                      = $Result.Name
                    StatusSeverityDescription = $Result.linestatuses.statusSeverityDescription
                    Reason                    = $Result.linestatuses.reason
                } # PSCustomObject

            } # foreach

        } # Disruption

    } # switch

    if ($Line) {
        if ($TFLTubeStatus -and $TFLTubeStatusRange) {

            $TFLTubeStatusRange
        } # if
        else {
            $TFLTubeStatus
        } # else
    } # if
    else {
        $TFLTubeDisruptions | Where-Object { $_.StatusSeverityDescription -ne 'Good Service' }
    } # else

} # function